variable "aws_access_key" {}
variable "aws_secret_key" {}

# The Global server prefix
variable "prefix" {
  default ="tst"
  description = "the global prefix for this build"
}

# naming servers by AZ
variable "sy1" {
  default ="sy1"
  description = "sy1 prefix"
}

variable "sy2" {
  default ="sy2"
  description = "sy2 prefix"
}

# AWS region
variable "aws_region" {
    description = "aws region to use"
    default = "ap-southeast-2"
}

# AWS Availability zones
variable "aws_sy1" {
    description = "sy1 Availability zones"
    default = "ap-southeast-2a"
}

variable "aws_sy2" {
    description = "sy1 Availability zones"
    default = "ap-southeast-2b"
}


# Key to use
variable "iamkey" {
	description = "The AWS .pem key to use"
	default = "puppet"

}