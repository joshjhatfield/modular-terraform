#!/bin/bash
# Make sure you run this as the default root account of the server at the time of provisioning. 
# Jhatfield 2016


# Vars



# Setup installers



# Functions and stuff

mastersetup ()
{
	mkdir home/ubuntu/terra-nullius
	# install basic utils
	sudo apt-get -y install git unzip software-properties-common
	sudo apt-add-repository -y ppa:ansible/ansible
	sudo apt-get update
	sudo apt-get -y install ansible openjdk-7-jre
	# Download and install elastic.
	cd home/ubuntu/terra-nullius
	wget https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/deb/elasticsearch/2.4.1/elasticsearch-2.4.1.deb
	sudo dpkg -i elasticsearch-2.4.1.deb
	service elasticsearch start
}

mastersetup