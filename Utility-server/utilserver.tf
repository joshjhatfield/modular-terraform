# Designed to be a general utility server. 
resource "aws_instance" "master" {
  count = 1
  ami = "ami-ba3e14d9"
  instance_type = "t2.micro"
  availability_zone = "${var.aws_sy1}"
  vpc_security_group_ids = ["${aws_security_group.serveraccess.id}", "${aws_security_group.utilaccess.id}"]
  key_name ="${var.iamkey}"
  #subnet_id = "${aws_subnet.jh_subnet1-sy1.id}"
  tags {
    Name = "${var.sy1}-${var.prefix}-app${count.index}"
  }
}
