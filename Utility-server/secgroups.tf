# access SG
resource "aws_security_group" "serveraccess" {
  name = "${var.prefix}-access-group"
  description ="Access to the servers for SSH and repos"
  #vpc_id = "${aws_vpc.main-jh.id}"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   egress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
resource "aws_security_group" "utilaccess" {
  name = "${var.prefix}-util-ports"
  description ="Utility ports for sever applications"
  #vpc_id = "${aws_vpc.main-jh.id}"
   ingress {
    from_port = 9200
    to_port = 9200
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  } 
}

