resource "aws_vpc" "main-jh" {
    cidr_block = "${var.vpc_cidr}"
    enable_dns_hostnames ="true"
      tags {
    Name = "${var.prefix}-tf-vpc"
  }
}



resource "aws_subnet" "jh_subnet1-sy1" {
    vpc_id = "${aws_vpc.main-jh.id}"
    cidr_block = "${var.jh_internal_sy1}"
    availability_zone = "ap-southeast-2a"

    tags {
        Name = "${var.prefix}-Private-sy1"
    }
}

resource "aws_subnet" "jh_subnet1-sy2" {
    vpc_id = "${aws_vpc.main-jh.id}"
    cidr_block = "${var.jh_internal_sy2}"
    availability_zone = "ap-southeast-2b"

    tags {
        Name = "${var.prefix}-Private-sy2"
    }
}