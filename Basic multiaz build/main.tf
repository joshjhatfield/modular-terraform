provider "aws" {
  region = "${var.aws_region}"
    access_key = "${var.aws_access_key}"
    secret_key = "${var.aws_secret_key}"
}



# app1
resource "aws_instance" "master" {
  count = 3
  ami = "ami-ba3e14d9"
  instance_type = "t2.micro"
  availability_zone = "ap-southeast-2a"
  vpc_security_group_ids = ["${aws_security_group.serveraccess.id}", "${aws_security_group.baloney.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.jh_subnet1-sy1.id}"


  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              EOF

  tags {
    Name = "${var.sy1}-${var.prefix}-app${count.index}"
  }
}

# app2

resource "aws_instance" "agent" {
  count = 3
  ami = "ami-ba3e14d9"
  instance_type = "t2.micro"
  availability_zone = "ap-southeast-2b"
  vpc_security_group_ids = ["${aws_security_group.serveraccess.id}", "${aws_security_group.baloney.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.jh_subnet1-sy2.id}"


  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              EOF

  tags {
    Name = "${var.sy2}-${var.prefix}-app${count.index}"
  }
}


