# access SG
resource "aws_security_group" "serveraccess" {
  name = "${var.prefix}-access-group"
  description ="Access to the servers for SSH and repos"
  vpc_id = "${aws_vpc.main-jh.id}"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   egress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
# Baloney group
resource "aws_security_group" "baloney" {
  name = "${var.prefix}-baloney-group"
  description ="A baloney group for POC, nothing of value here"
  vpc_id = "${aws_vpc.main-jh.id}"
  ingress {
    from_port = 2342
    to_port = 2342
    protocol = "tcp"
    cidr_blocks = ["8.8.8.8/32"]
  }
   egress {
    from_port = 8014
    to_port = 8014
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
}
}