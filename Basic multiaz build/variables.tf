variable "aws_access_key" {}
variable "aws_secret_key" {}

# IAM key name
variable "iamkey" {
  default ="puppet"
  description = "name of IAM key being used"
}

# The Global server prefix
variable "prefix" {
  default ="tst"
  description = "the global prefix for this build"
}

# naming servers by AZ
variable "sy1" {
  default ="sy1"
  description = "sy1 prefix"
}

variable "sy2" {
  default ="sy2"
  description = "sy2 prefix"
}

# AWS region
variable "aws_region" {
    description = "aws region to use"
    default = "ap-southeast-2"
}

# AWS Availability zones
variable "aws_sy1" {
    description = "sy1 Availability zones"
    default = "ap-southeast-2a"
}

variable "aws_sy2" {
    description = "sy1 Availability zones"
    default = "ap-southeast-2b"
}



# network subnets and vpc
variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.15.0.0/16"
}

variable "jh_internal_sy1" {
    description = "CIDR for sy1 internal subnet"
    default = "10.15.1.0/24"
}

variable "jh_internal_sy2" {
    description = "CIDR for sy2 internal subnet"
    default = "10.15.2.0/24"
}

