# Bastion server subnet

resource "aws_subnet" "bastion_subnet_sy1" {
    vpc_id = "${aws_vpc.main-jh.id}"
    cidr_block = "${var.bastion_sy1}"
    availability_zone = "${var.aws_sy1}"
    map_public_ip_on_launch = true
    depends_on = ["aws_internet_gateway.jh-gw"]
    tags {
        Name = "${var.prefix}-Bastion-sy1"
    }
}

# Node subnets

resource "aws_subnet" "node_subnet-sy1" {
    vpc_id = "${aws_vpc.main-jh.id}"
    cidr_block = "${var.node_sy1}"
    availability_zone = "${var.aws_sy1}"

    tags {
        Name = "${var.prefix}-node-sy1"
    }
}

resource "aws_subnet" "node_subnet-sy2" {
    vpc_id = "${aws_vpc.main-jh.id}"
    cidr_block = "${var.node_sy2}"
    availability_zone = "${var.aws_sy2}"

    tags {
        Name = "${var.prefix}-node-sy2"
    }
}

# Master subnets

resource "aws_subnet" "master_subnet-sy1" {
    vpc_id = "${aws_vpc.main-jh.id}"
    cidr_block = "${var.master_sy1}"
    availability_zone = "${var.aws_sy1}"

    tags {
        Name = "${var.prefix}-master-sy1"
    }
}

resource "aws_subnet" "master_subnet-sy2" {
    vpc_id = "${aws_vpc.main-jh.id}"
    cidr_block = "${var.master_sy2}"
    availability_zone = "${var.aws_sy2}"

    tags {
        Name = "${var.prefix}-master-sy2"
    }
}

# Infra subnets

resource "aws_subnet" "infra_subnet-sy1" {
    vpc_id = "${aws_vpc.main-jh.id}"
    cidr_block = "${var.infra_sy1}"
    availability_zone = "${var.aws_sy1}"

    tags {
        Name = "${var.prefix}-infra-sy1"
    }
}

resource "aws_subnet" "infra_subnet-sy2" {
    vpc_id = "${aws_vpc.main-jh.id}"
    cidr_block = "${var.infra_sy2}"
    availability_zone = "${var.aws_sy2}"

    tags {
        Name = "${var.prefix}-infra-sy2"
    }
}

# Adm server subnet

resource "aws_subnet" "adm_subnet-sy1" {
    vpc_id = "${aws_vpc.main-jh.id}"
    cidr_block = "${var.adm_sy1}"
    availability_zone = "${var.aws_sy1}"

    tags {
        Name = "${var.prefix}-adm-sy1"
    }
}

# RDS Subnet

resource "aws_db_subnet_group" "db_subnet-grp" {
    name = "main"
    subnet_ids = ["${aws_subnet.rds_subnet-sy1.id}", "${aws_subnet.rds_subnet-sy2.id}"]
    tags {
        Name = "${var.prefix}-db-subgrp"
    }
}

resource "aws_subnet" "rds_subnet-sy1" {
    vpc_id = "${aws_vpc.main-jh.id}"
    cidr_block = "${var.rds_sy1}"
    availability_zone = "${var.aws_sy1}"

    tags {
        Name = "${var.prefix}-rds-sy1"
    }
}

resource "aws_subnet" "rds_subnet-sy2" {
    vpc_id = "${aws_vpc.main-jh.id}"
    cidr_block = "${var.rds_sy2}"
    availability_zone = "${var.aws_sy2}"

    tags {
        Name = "${var.prefix}-rds-sy1"
    }
}