

# Node Sy1

resource "aws_instance" "sy1node" {
  #count = 2
  ami = "${var.cent-ami}"
  instance_type = "${var.awsm4large}"
  availability_zone = "ap-southeast-2a"
  vpc_security_group_ids = ["${aws_security_group.serveraccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.node_subnet-sy1.id}"
  
  root_block_device = {
  volume_type = "standard"
  volume_size = "30"
 } 

  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.sy1}${var.prefix}-node${count.index + 1}
              sudo echo "${var.sy1}${var.prefix}-node${count.index + 1}" > /etc/hostname
              EOF

  tags {
    Name = "${var.sy1}${var.prefix}-node${count.index + 1}"
  }
}


# Node Sy2

resource "aws_instance" "sy2node" {
  #count = 2
  ami = "${var.cent-ami}"
  instance_type = "${var.awsm4large}"
  availability_zone = "ap-southeast-2b"
  vpc_security_group_ids = ["${aws_security_group.serveraccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.node_subnet-sy2.id}"
  
  root_block_device = {
  volume_type = "standard"
  volume_size = "30"
 } 

  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.sy2}${var.prefix}-node${count.index + 1}
              sudo cat ${var.sy2}${var.prefix}-node${count.index + 1} > /etc/hostname
              EOF

  tags {
    Name = "${var.sy2}${var.prefix}-node${count.index + 1}"
  }
}