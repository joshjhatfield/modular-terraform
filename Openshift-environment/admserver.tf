# Adm server, puppet, ansible, other stuff. 

resource "aws_instance" "admin" {
  #count = 1 # no need for this as there will only be one puppet master. 
  ami = "${var.node-ami}"
  instance_type = "t2.micro"
  availability_zone = "ap-southeast-2a"
  vpc_security_group_ids = ["${aws_security_group.serveraccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.adm_subnet-sy1.id}"


  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.sy1}${var.prefix}-adm${count.index + 1}
              sudo echo "${var.sy1}${var.prefix}-adm${count.index + 1}" > /etc/hostname
              EOF

  tags {
    Name = "${var.sy1}${var.prefix}-adm${count.index + 1}"
  }
}