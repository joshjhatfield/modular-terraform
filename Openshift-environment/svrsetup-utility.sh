#!/bin/bash
# Make sure you run this as the default root account of the server at the time of provisioning, Usually 
# the ubuntu or centos user on a default AMI  
# This is not used by the openshift installer, use this on your
# Jhatfield 2016


# Vars



# Setup installers



# Functions and stuff

elastic ()
{
	sudo mkdir home/ubuntu/setupdir
	# install basic utils
	sudo apt-get -y install git unzip software-properties-common
	sudo apt-add-repository -y ppa:ansible/ansible
	sudo apt-get update
	sudo apt-get -y install ansible openjdk-7-jre
	# Download and install elastic for ubuntu servers
	cd home/ubuntu/setupdir
	wget https://download.elastic.co/elasticsearch/release/org/elasticsearch/distribution/deb/elasticsearch/2.4.1/elasticsearch-2.4.1.deb
	sudo dpkg -i elasticsearch-2.4.1.deb
	service elasticsearch start
}

ansible ()
{
	sudo apt-get -y install software-properties-common
	sudo apt-add-repository -y ppa:ansible/ansible
	sudo apt-get update
	sudo apt-get -y install git ansible openjdk-7-jre
}

terraform ()
{
	sudo apt-get -y install git unzip
	wget https://releases.hashicorp.com/terraform/0.7.7/terraform_0.7.7_linux_amd64.zip
	unzip terraform_0.7.7_linux_amd64.zip
	sudo mv terraform /usr/local/bin/
}

# Pos Vars

if [[ $1 == elastic ]]; then
	elastic
fi

if [[ $1 == ansible ]]; then
	ansible
fi

if [[ $1 == terraform ]]; then
	terraform
fi