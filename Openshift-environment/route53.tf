
resource "aws_route53_zone" "ocluster" {
  name = "${var.privdns}"
  vpc_id = "${aws_vpc.main-jh.id}"
  force_destroy = "true"
  tags {
  	Name = "${var.privdns}"
  }
}

###############
## A RECORDS ##
###############

# bastion server record
resource "aws_route53_record" "sy1bastionrecord" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.sy1}${var.prefix}-bastion${count.index + 1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.bastionbox.private_ip}"]
}
# admin server record
resource "aws_route53_record" "sy1admrecord" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.sy1}${var.prefix}-adm${count.index + 1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.admin.private_ip}"]
}
# master record
resource "aws_route53_record" "sy1masterrecord" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.sy1}${var.prefix}-master${count.index + 1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.sy1master.private_ip}"]
}
# node record, experimental
resource "aws_route53_record" "sy1noderecord" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.sy1}${var.prefix}-node${count.index + 1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.sy1node.private_ip}"]
}
resource "aws_route53_record" "sy2noderecord" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.sy2}${var.prefix}-node${count.index + 1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.sy2node.private_ip}"]
}
# Infra record
resource "aws_route53_record" "sy1infrarecord" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.sy1}${var.prefix}-infra${count.index + 1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.sy1infra.private_ip}"]
}
resource "aws_route53_record" "sy2infrarecord" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.sy2}${var.prefix}-infra${count.index + 1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.sy2infra.private_ip}"]
}