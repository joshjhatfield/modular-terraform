#!/bin/bash
# an all in one provisioner script for the various servers in an environment.
# these are executed with the required variables via ansible playbooks in this directory. 
# Possibly will look at running an all in one script to run the playbooks later. 
# Use the required playbook for the server and this script will select the parts it needs. 


# Functions

test ()
{
	echo "hello world" >> ~/helloworld.txt
}


# Pos Vars

if [[ $1 == test ]]; then
	test
fi