# LB/infra SY1

resource "aws_instance" "sy1infra" {
  #count = 1
  ami = "${var.cent-ami}"
  instance_type = "t2.micro"
  availability_zone = "ap-southeast-2a"
  vpc_security_group_ids = ["${aws_security_group.serveraccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.infra_subnet-sy1.id}"

  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.sy1}${var.prefix}-infra${count.index + 1}
              sudo echo "${var.sy1}${var.prefix}-infra${count.index + 1}" > /etc/hostname
              EOF

  tags {
    Name = "${var.sy1}${var.prefix}-infra${count.index + 1}"
  }
}

# LB/infra SY2

resource "aws_instance" "sy2infra" {
  #count = 1
  ami = "${var.cent-ami}"
  instance_type = "t2.micro"
  availability_zone = "ap-southeast-2b"
  vpc_security_group_ids = ["${aws_security_group.serveraccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.infra_subnet-sy2.id}"

  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.sy2}${var.prefix}-infra${count.index + 1}
              sudo echo "${var.sy2}${var.prefix}-infra${count.index + 1}" > /etc/hostname
              EOF

  tags {
    Name = "${var.sy2}${var.prefix}-infra${count.index + 1}"
  }
}