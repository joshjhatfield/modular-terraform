# Bastion Sy1

resource "aws_instance" "bastionbox" {
  count = 1
  ami = "${var.bastion-ami}"
  instance_type = "t2.micro"
  availability_zone = "${var.aws_sy1}"
  vpc_security_group_ids = ["${aws_security_group.bastionaccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.bastion_subnet_sy1.id}"


  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.sy1}${var.prefix}-bastion${count.index + 1}
              sudo echo "${var.sy1}${var.prefix}-bastion${count.index + 1}" > /etc/hostname
              EOF

  tags {
    Name = "${var.sy1}${var.prefix}-bastion${count.index + 1}"
  }
}