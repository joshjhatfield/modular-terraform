# Master Sy1

resource "aws_instance" "sy1master" {
  #count = 1
  ami = "${var.cent-ami}"
  instance_type = "${var.awsr3large}"
  availability_zone = "ap-southeast-2a"
  vpc_security_group_ids = ["${aws_security_group.serveraccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.node_subnet-sy1.id}"
  #private_dns = "${var.sy1}${var.prefix}-master${count.index + 1}.dev.oscluster.com"
 
  root_block_device = {
  volume_type = "standard"
  volume_size = "50"
 } 


  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.sy1}${var.prefix}-master${count.index + 1}
              sudo echo "${var.sy1}${var.prefix}-master${count.index + 1}" > /etc/hostname
              EOF

  tags {
    Name = "${var.sy1}${var.prefix}-master${count.index + 1}"
  }
}