resource "aws_network_acl" "internal" {
    vpc_id = "${aws_vpc.main-jh.id}"
    subnet_ids= ["${aws_subnet.node_subnet-sy1.id}", "${aws_subnet.node_subnet-sy2.id}"]
    subnet_ids= ["${aws_subnet.adm_subnet-sy1.id}", "${aws_subnet.master_subnet-sy1.id}"]
    subnet_ids= ["${aws_subnet.master_subnet-sy2.id}", "${aws_subnet.infra_subnet-sy1.id}"]
    subnet_ids= ["${aws_subnet.infra_subnet-sy2.id}", "${aws_subnet.rds_subnet-sy1.id}"]
    subnet_ids= ["${aws_subnet.rds_subnet-sy2.id}"]

    ingress {
        protocol = "-1"
        rule_no = 100
        action = "allow"
        cidr_block =  "0.0.0.0/0"
        from_port = 0
        to_port = 0
    }

    egress {
        protocol = "-1"
        rule_no = 100
        action = "allow"
        cidr_block =  "0.0.0.0/0"
        from_port = 0
        to_port = 0
    }

    tags {
        Name = "${var.prefix}-main-acl"
    }
}
