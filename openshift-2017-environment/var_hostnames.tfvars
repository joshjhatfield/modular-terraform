# Includes hostname and aws utilies names configs as vars



# Util server hostnames
variable "util1" {
	description = "util server 1"
	default = "${var.az1}${var.prefix}-util${count.index + 1}"
}

variable "util2" {
	description = "util server 2"
	default = "${var.az2}${var.prefix}-util${count.index + 1}"
}

# App server hostnames

variable "app1" {
	description = "app server 1"
	default = "${var.az1}${var.prefix}-app${count.index + 1}"
}

variable "app2" {
	description = "app server 2"
	default = "${var.az2}${var.prefix}-app${count.index + 1}"
}

# MQ Servers



variable "mq1" {
	description = "mq server 1"
	default = "${var.az1}${var.prefix}-mq${count.index + 1}"
}

variable "mq2" {
	description = "mq server 2"
	default = "${var.az2}${var.prefix}-mq${count.index + 1}"
}



# Web servers


variable "web1" {
	description = "web server 1"
	default = "${var.az1}${var.prefix}-web${count.index + 1}"
}

variable "web2" {
	description = "web server 2"
	default = "${var.az2}${var.prefix}-web${count.index + 1}"
}



# nat servers

variable "nat1" {
	description = "nat server 1"
	default = "${var.az1}${var.prefix}-nat${count.index + 1}"
}

# Ldap servers



variable "ldap1" {
	description = "ldap server 1"
	default = "${var.az1}${var.prefix}-ldap${count.index + 1}"
}


# reporting servers



variable "report1" {
	description = "reporting server 1"
	default = "${var.az1}${var.prefix}-report${count.index + 1}"
}

variable "report2" {
	description = "reporting server 2"
	default = "${var.az2}${var.prefix}-report${count.index + 1}"
}





# Subnet names

# Core subnets

variable "coreaz1sub" {
	description = "az1 core subnet"
	default = "${var.prefix}-core-az1"
}

variable "coreaz2sub" {
	description = "az2 core subnet"
	default = "${var.prefix}-core-az2"
}



# Security group names

# Core Secgroup

variable "coreaccesssg" {
	description = "Core Access security group"
	default = "${var.prefix}-core-servers"
}

variable "appaccesssg" {
	description = "App server security group"
	default = "${var.prefix}-app-servers"
}

variable "webaccesssg" {
	description = "web server security group"
	default = "${var.prefix}-web-servers"
}


variable "mqaccesssg" {
	description = "mq server security group"
	default = "${var.prefix}-mq-servers"
}


variable "nataccesssg" {
	description = "nat server security group"
	default = "${var.prefix}-nat-servers"
}

variable "ldapaccesssg" {
	description = "ldap server security group"
	default = "${var.prefix}-ldap-servers"
}

variable "reportaccesssg" {
	description = "report server security group"
	default = "${var.prefix}-report-servers"
}