# internal SG
resource "aws_security_group" "coreaccess" {
  name = "${var.coreaccesssg}"
  description ="Access to the core servers"
  vpc_id = "${aws_vpc.main-jh.id}"
  # alter this rule post install to harden environment.
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["10.15.0.0/16"]
  }
   egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   egress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}



# app access

resource "aws_security_group" "appaccess" {
  name = "${var.appaccesssg}"
  description ="Access to the app servers"
  vpc_id = "${aws_vpc.main-jh.id}"
  # alter this rule post install to harden environment.
  ingress {
    from_port = 10101
    to_port = 10150
    protocol = "tcp"
    cidr_blocks = ["10.15.0.0/16"]  # Replace with web SG
  }
}


# Web server access

resource "aws_security_group" "webaccess" {
  name = "${var.webaccesssg}"
  description ="Access to the web servers"
  vpc_id = "${aws_vpc.main-jh.id}"
  # alter this rule post install to harden environment.
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  
  }
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  
  }
  egress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  
  }
  egress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  
  }
}



# MQ server access

resource "aws_security_group" "mqaccess" {
  name = "${var.mqaccesssg}"
  description ="Access to the mq servers"
  vpc_id = "${aws_vpc.main-jh.id}"
  # alter this rule post install to harden environment.
  ingress {
    from_port = 5672
    to_port = 5672
    protocol = "tcp"
    cidr_blocks = ["10.15.0.0/16"]  
  }
  ingress {
    from_port = 15672
    to_port = 15672
    protocol = "tcp"
    cidr_blocks = ["10.15.0.0/16"]  
  }
  egress {
    from_port = 5672
    to_port = 5672
    protocol = "tcp"
    cidr_blocks = ["10.15.0.0/16"]  
  }
}



# nat server access

resource "aws_security_group" "nataccess" {
  name = "${var.nataccesssg}"
  description ="Access to the nat servers"
  vpc_id = "${aws_vpc.main-jh.id}"
  # alter this rule post install to harden environment.
  ingress {
    from_port = 0
    to_port = 65353
    protocol = "tcp"
    cidr_blocks = ["10.15.0.0/16"]  
  }
  ingress {
    from_port = 0
    to_port = 65353
    protocol = "udp"
    cidr_blocks = ["10.15.0.0/16"]  
  }
  egress {
    from_port = 0
    to_port = 65353
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  
  }
egress {
    from_port = 0
    to_port = 65353
    protocol = "udp"
    cidr_blocks = ["0.0.0.0/0"]  
  }
}


# ldap access

resource "aws_security_group" "ldapaccess" {
  name = "${var.ldapaccesssg}"
  description ="Access to the ldap servers"
  vpc_id = "${aws_vpc.main-jh.id}"
  # alter this rule post install to harden environment.
  ingress {
    from_port = 389
    to_port = 389
    protocol = "tcp"
    cidr_blocks = ["10.15.0.0/16"]  
  }
  ingress {
    from_port = 389
    to_port = 389
    protocol = "udp"
    cidr_blocks = ["10.15.0.0/16"]  
  }
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["10.15.0.0/16"]  
  }
  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["10.15.0.0/16"]  
  }
}


# ldap access

resource "aws_security_group" "reportaccess" {
  name = "${var.reportaccesssg}"
  description ="Access to the reporting servers"
  vpc_id = "${aws_vpc.main-jh.id}"
  # alter this rule post install to harden environment.
  ingress {
    from_port = 9200
    to_port = 9200
    protocol = "tcp"
    cidr_blocks = ["10.15.0.0/16"]  
  }
  ingress {
    from_port = 9300
    to_port = 9300
    protocol = "tcp"
    cidr_blocks = ["10.15.0.0/16"]  
  }
}



