variable "aws_access_key" {}
variable "aws_secret_key" {}

########################
## AWS GLOBAL SETTINGS##
########################

# The Global server prefix
variable "prefix" {
  default ="jjh"
  description = "the global prefix for this build"
}

# naming servers by AZ
variable "az1" {
  default ="sy1"
  description = "Availability zone 1 prefix"
}

variable "az2" {
  default ="sy2"
  description = "Availability zone 2 prefix"
}

# The private DNS record
variable "privdns" {
  default = "dev.oscluster.com"
  description = "The global private dns record for route53"
}

# AWS region
variable "aws_region" {
    description = "aws region to use"
    default = "ap-southeast-2"
}

# AWS Availability zones
variable "aws_az1" {
    description = "az1 Availability zones"
    default = "ap-southeast-2a"
}

variable "aws_az2" {
    description = "az2 Availability zones"
    default = "ap-southeast-2b"
}

# IAM key name
variable "iamkey" {
  default ="puppet"
  description = "name of IAM key being used"
}

#############################
## Openshift Server Config ##
#############################
## AMI'S

# Bastionserver
variable "bastion-ami" {
  description= " Ubuntu 14.04 AMI for bastion"
  default= "ami-ba3e14d9"
}

# Nodeconfig
variable "node-ami" {
  description= "Ubuntu 14.04 AMI for General purpose"
  default= "ami-ba3e14d9"
}

# Centos AMI
variable "cent-ami" {
  description= "The centos AMI for openshift"
  default= "ami-fedafc9d"
}

## Machine sizes
variable "awst2micro" {
  description= "t2.micro AMI size"
  default= "t2.micro"
}

variable "awsm3medium" {
  description= "m3.medium instance size 1xcpu, 4GB ram"
  default= "m3.medium"
}

variable "awsm4large" {
  description= "m4.large instance size 2xcpu, 8GB ram"
  default= "m4.large"
}

variable "awsr3large" {
  description= "r3.large instance size 2xcpu, 15GB ram mem oprimized"
  default= "r3.large"
}


#######################
##  VPC AND SUBNETS  ##
#######################
# network subnets and vpc
variable "vpc_cidr" {
    description = "CIDR for the whole VPC"
    default = "10.15.0.0/16"
}
# bastion
variable "bastion_sy1" {
    description = "CIDR for sy2 internal bastion subnet"
    default = "10.15.5.0/24"
}

# nodes
variable "node_az1" {
    description = "CIDR for sy1 internal node subnet"
    default = "10.15.10.0/24"
}

variable "node_az2" {
    description = "CIDR for sy2 internal node subnet"
    default = "10.15.11.0/24"
}
# masters
variable "master_sy1" {
    description = "CIDR for sy1 internal master subnet"
    default = "10.15.20.0/24"
}

variable "master_sy2" {
    description = "CIDR for sy2 internal master subnet"
    default = "10.15.21.0/24"
}
# Infra
variable "infra_sy1" {
    description = "CIDR for sy1 internal infra subnet"
    default = "10.15.30.0/24"
}

variable "infra_sy2" {
    description = "CIDR for sy2 internal infra subnet"
    default = "10.15.31.0/24"
}
# adm server
variable "adm_sy1" {
  description = "the subnet for the puppet and ansible servers"
  default = "10.15.6.0/24"
}

# RDS db subnets

variable "rds_sy1" {
  description = "the subnet for RDS az 1"
  default = "10.15.98.0/24"
}

variable "rds_sy2" {
  description = "the subnet for RDS az 2"
  default = "10.15.99.0/24"
}


