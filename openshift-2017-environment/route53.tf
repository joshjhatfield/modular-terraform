resource "aws_route53_zone" "ocluster" {
  name = "${var.privdns}"
  vpc_id = "${aws_vpc.main-jh.id}"
  force_destroy = "true"
  tags {
  	Name = "${var.privdns}"
  }
}

###############
## A RECORDS ##
###############

# util server records
resource "aws_route53_record" "util1dns" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.util1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az1util.private_ip}"]
}


resource "aws_route53_record" "util2dns" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.util2}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az2util.private_ip}"]
}


# app server records
resource "aws_route53_record" "app1dns" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.app1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az1app.private_ip}"]
}


resource "aws_route53_record" "app2dns" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.app2}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az2app.private_ip}"]
}


# MQ server records
resource "aws_route53_record" "mq1dns" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.mq1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az1mq.private_ip}"]
}


resource "aws_route53_record" "mq2dns" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.mq2}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az2mq.private_ip}"]
}


# web server records
resource "aws_route53_record" "web1dns" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.web1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az1web.private_ip}"]
}


resource "aws_route53_record" "web2dns" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.web2}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az2web.private_ip}"]
}


# nat server records
resource "aws_route53_record" "nat1dns" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.nat1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az1nat.private_ip}"]
}


# ldap server records
resource "aws_route53_record" "ldap1dns" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.ldap1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az1ldap.private_ip}"]
}

# report server records
resource "aws_route53_record" "report1dns" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.report1}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az1report.private_ip}"]
}

resource "aws_route53_record" "report2dns" {
 zone_id = "${aws_route53_zone.ocluster.zone_id}"
 name = "${var.report2}"
 type = "A"
 ttl = "300"
 records = ["${aws_instance.az2report.private_ip}"]
}


