# NAT servers for main env


resource "aws_instance" "az1nat" {
  ami = "${var.cent-ami}"
  instance_type = "${var.awsm3medium}"
  availability_zone = "${var.aws_az1}"
  vpc_security_group_ids = ["${aws_security_group.coreaccess.id}", "${aws_security_group.nataccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.core_subnet-az1.id}"

  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.nat1}
              sudo echo "${var.nat1}" > /etc/hostname
              EOF

  tags {
    Name = "${var.nat1}"
  }
}