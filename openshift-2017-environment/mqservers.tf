# MQ servers for main env


resource "aws_instance" "az1mq" {
  ami = "${var.cent-ami}"
  instance_type = "${var.awsm3medium}"
  availability_zone = "${var.aws_az1}"
  vpc_security_group_ids = ["${aws_security_group.coreaccess.id}", "${aws_security_group.mqaccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.core_subnet-az1.id}"

  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.mq1}
              sudo echo "${var.mq1}" > /etc/hostname
              EOF

  tags {
    Name = "${var.mq1}"
  }
}

resource "aws_instance" "az2mq" {
  ami = "${var.cent-ami}"
  instance_type = "${var.awsm3medium}"
  availability_zone = "${var.aws_az2}"
  vpc_security_group_ids = ["${aws_security_group.coreaccess.id}", "${aws_security_group.mqaccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.core_subnet-az2.id}"

  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.mq2}
              sudo echo "${var.mq2}" > /etc/hostname
              EOF

  tags {
    Name = "${var.mq2}"
  }
}