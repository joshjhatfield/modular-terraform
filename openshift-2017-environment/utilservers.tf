# util server

resource "aws_instance" "az1util" {
  ami = "${var.cent-ami}"
  instance_type = "${var.awsm4large}"
  availability_zone = "${var.aws_az1}"
  vpc_security_group_ids = ["${aws_security_group.coreaccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.core_subnet-az1.id}"

  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.util1}
              sudo echo "${var.util1}" > /etc/hostname
              EOF

  tags {
    Name = "${var.util1}"
  }
}

resource "aws_instance" "az2util" {
  ami = "${var.cent-ami}"
  instance_type = "${var.awsm4large}"
  availability_zone = "${var.aws_az2}"
  vpc_security_group_ids = ["${aws_security_group.coreaccess.id}"]
  key_name ="${var.iamkey}"
  subnet_id = "${aws_subnet.core_subnet-az2.id}"

  user_data = <<-EOF
              #!/bin/bash
              mkdir /opt/jh-services
              echo "Hello, World" > /opt/jh-services/index.html
              sudo hostname ${var.util2}
              sudo echo "${var.util2}" > /etc/hostname
              EOF

  tags {
    Name = "${var.util2}"
  }
}