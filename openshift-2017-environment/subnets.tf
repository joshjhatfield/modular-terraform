

# All server subnets kept here





# Core server internal subnet
resource "aws_subnet" "core_subnet-az1" {
    vpc_id = "${aws_vpc.main-jh.id}"
    cidr_block = "${var.node_az1}"
    availability_zone = "${var.aws_az1}"

    tags {
        Name = "${var.coreaz1sub}"
    }
}

resource "aws_subnet" "core_subnet-az2" {
    vpc_id = "${aws_vpc.main-jh.id}"
    cidr_block = "${var.node_az2}"
    availability_zone = "${var.aws_az2}"

    tags {
        Name = "${var.coreaz2sub}"
    }
}